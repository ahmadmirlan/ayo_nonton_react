import {lazy} from 'react';
import Login from './pages/Login';
import Register from './pages/Register';

const routes = [
    /* Dashboard Pages */
    {
        path: '/dashboard/categories',
        component: lazy(() => import('./pages/Dashboard/Categories/Categories')),
    },
    {
        path: '/dashboard/movies/basic',
        component: lazy(() => import('./pages/Dashboard/Movies/Basic')),
    },
    {
        path: '/dashboard/movies/edit/:movieId',
        component: lazy(() => import('./pages/Dashboard/Movies/AddMovie')),
    },
    {
        path: '/dashboard/movies/create',
        component: lazy(() => import('./pages/Dashboard/Movies/AddMovie')),
    },
    {
        path: '/dashboard/movies',
        component: lazy(() => import('./pages/Dashboard/Movies/Movies')),
    },
    {
        path: '/dashboard/list/movies',
        component: lazy(() => import('./pages/Dashboard/Movies/MovieList')),
    },
    {
        path: '/dashboard/users',
        component: lazy(() => import('./pages/Dashboard/User/Users')),
    },
    {
        path: '/dashboard/home',
        component: lazy(() => import('./pages/Dashboard/index')),
    },
    /* Authentication Pages*/
    {
        path: '/auth/register',
        component: Register,
        isPublic: true
    },
    {
        path: '/auth/login',
        component: Login,
        isPublic: true
    },
    {
        path: '/',
        component: lazy(() => import('./pages/Dashboard/index')),
    }
];

export default routes;
