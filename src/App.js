import React, {Suspense} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import routes from './routes';
import LayoutWrapper from "./components/Layout";
import Loader from "./components/Loader/Loader";
import Boot from './store/boot.js';
import Translation from './i18n';

function withLayout(WrappedComponent) {
    // ...and returns another component...
    return class extends React.Component {
        render() {
            return <LayoutWrapper>
                <WrappedComponent/>
            </LayoutWrapper>
        }
    };
}

const PrivateRoute = ({component: Component, ...rest}) => (
    <Suspense fallback={<Loader/>}>
        <Route {...rest} render={(props) => (
            <Component {...props} />
        )}/>
    </Suspense>
);


function App() {
    const lang = localStorage.getItem('lang');
    Translation(lang ? lang : 'en');
    return (
        <React.Fragment>
            <Router>
                <Switch>
                    {routes.map((route, idx) =>
                        route.isPublic ?
                            <Route path={route.path} component={route.component} key={idx}/> :
                            <PrivateRoute path={route.path} component={withLayout(route.component)} key={idx}/>
                    )}
                </Switch>
            </Router>
        </React.Fragment>
    );
}

Boot().then(() => App);

export default App;
