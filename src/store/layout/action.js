const layoutAction = {
    SIDEBAR_COLLAPSED: 'SideBar Collapsed',

    sideBarCollapsed: (isCollapsed) => {
        return {
            type: layoutAction.SIDEBAR_COLLAPSED,
            payload: {isCollapsed}
        }
    }
};

export default layoutAction;
