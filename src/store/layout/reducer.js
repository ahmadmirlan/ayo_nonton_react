import layoutAction from './action'
const initLayoutState = {
    isCollapsed: false,
};

export default function layoutReducer(state = initLayoutState, {type, payload}) {
    if (type === layoutAction.SIDEBAR_COLLAPSED) {
        return {
            ...state,
            isCollapsed: payload.isCollapsed
        };
    } else {
        return {
            ...state
        }
    }
}
