import {all} from 'redux-saga/effects'
import authSaga from './auth/saga';
import categorySaga from './category/saga';
import userSaga from './users/saga';
import movieSaga from './movie/saga';

export default function* rootSaga() {
    yield all([
        authSaga(),
        categorySaga(),
        userSaga(),
        movieSaga()
    ])
}
