import {all, call, put, takeEvery} from 'redux-saga/effects';
import {createCategory, deleteCategory, getAllCategories, updateCategory} from '../../helpers/category.helper';
import categoryAction from './action';
import {message} from 'antd';

function* loadAllCategories() {
    try {
        yield put(categoryAction.loadCategoryPageToggleLoading(true));
        const response = yield call(getAllCategories);
        yield put(categoryAction.loadCategoriesSuccess(response));
        yield put(categoryAction.loadCategoryPageToggleLoading(false));
    } catch (e) {
        yield put(categoryAction.loadCategoryPageToggleLoading(false));
    }
}

function* updateCategoryWatch({payload}) {
    try {
        yield put(categoryAction.loadCategoryPageActionLoading(true));
        const response = yield call(updateCategory, payload.category);
        yield put(categoryAction.updateCategorySuccess(response));
        yield put(categoryAction.loadCategoryPageActionLoading(false));
        message.success('Update category success!');
    } catch (e) {
        yield put(categoryAction.loadCategoryPageActionLoading(false));
        message.error('Update category failed!');
    }
}

function* createCategoryWatch({payload}) {
    try {
        yield put(categoryAction.loadCategoryPageActionLoading(true));
        const response = yield call(createCategory, payload.category);
        yield put(categoryAction.createCategorySuccess(response));
        message.success(`Category "${response.name}" created!`);
        yield put(categoryAction.loadCategoryPageActionLoading(true));
    } catch (e) {
        yield put(categoryAction.loadCategoryPageActionLoading(false));
        message.error(`Creating category failed`);
    }
}

function* deleteCategoryWatch({payload}) {
    try {
        yield put(categoryAction.loadCategoryPageActionLoading(true));
        yield call(deleteCategory, payload.category.id);
        yield put(categoryAction.deleteCategorySuccess(payload.category));
        message.success(`Category "${payload.category.name}" deleted!`);
        yield put(categoryAction.loadCategoryPageActionLoading(false));
    } catch (e) {
        yield put(categoryAction.loadCategoryPageActionLoading(false));
        message.error(`Failed deleting category!`);
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(categoryAction.LOAD_CATEGORIES_REQUESTED, loadAllCategories),
        takeEvery(categoryAction.UPDATE_CATEGORY_REQUESTED, updateCategoryWatch),
        takeEvery(categoryAction.DELETE_CATEGORY_REQUESTED, deleteCategoryWatch),
        takeEvery(categoryAction.CREATE_CATEGORY_REQUESTED, createCategoryWatch)
    ])
}
