import categoryAction from "./action";
import produce from "immer";

const initCategoryState = {
    isLoading: false,
    actionLoading: false,
    categories: [],
    category: {},
};

export default function categoryReducer(state = initCategoryState, {type, payload}) {
    switch (type) {
        case categoryAction.LOAD_CATEGORY_PAGE_TOGGLE_LOADING:
            return {
                ...state,
                isLoading: payload.loading
            };
        case categoryAction.LOAD_CATEGORY_PAGE_ACTION_LOADING:
            return {
                ...state,
                actionLoading: payload.loading
            };
        case categoryAction.LOAD_CATEGORIES_SUCCESS:
            return {
                ...state,
                isLoading: false,
                categories: payload.items,
            };
        case categoryAction.UPDATE_CATEGORY_SUCCESS:
            let categoryList = produce(state.categories, draftState => {
            });
            categoryList = categoryList.map(category => {
                if (category.id === payload.category.id) {
                    category = payload.category;
                    return category;
                }
                return category;
            });

            return {
                ...state,
                categories: categoryList
            };
        case categoryAction.DELETE_CATEGORY_SUCCESS:
            let categoryListDelete = produce(state.categories, draftState => {
                const catIndex = draftState.findIndex(cat => cat.id === payload.category.id);
                if (catIndex > -1) {
                    draftState.splice(catIndex, 1);
                }
            });
            return {
                ...state,
                categories: categoryListDelete
            };
        case categoryAction.CREATE_CATEGORY_SUCCESS:
            return {
                ...state,
                categories: produce(state.categories, draftState => {
                    draftState.push(payload.category);
                })
            };
        default:
            return {
                ...state
            }
    }
}
