const categoryAction = {
    LOAD_CATEGORY_PAGE_TOGGLE_LOADING: '[Load Category] Load Category Toggle Loading',
    LOAD_CATEGORY_PAGE_ACTION_LOADING: '[Load Category] Load Category Action Loading',
    LOAD_CATEGORIES_REQUESTED: '[Categories Page] Load Categories Requested',
    LOAD_CATEGORIES_SUCCESS: '[Categories Page] Load Categories Success',
    UPDATE_CATEGORY_REQUESTED: '[Category] Update Category Requested',
    UPDATE_CATEGORY_SUCCESS: '[Category] Update Category Success',
    DELETE_CATEGORY_REQUESTED: '[Category] Delete Category Requested',
    DELETE_CATEGORY_SUCCESS: '[Category] Delete Category Success',
    CREATE_CATEGORY_REQUESTED: '[Category] Create Category Requested',
    CREATE_CATEGORY_SUCCESS: '[Category] Create Category Success',

    loadCategoryPageToggleLoading: (isLoading) => {
        return {
            type: categoryAction.LOAD_CATEGORY_PAGE_TOGGLE_LOADING,
            payload: {isLoading}
        }
    },

    loadCategoryPageActionLoading: (isLoading) => {
        return {
            type: categoryAction.LOAD_CATEGORY_PAGE_ACTION_LOADING,
            payload: {isLoading}
        }
    },

    loadCategoriesRequested: () => {
        return {
            type: categoryAction.LOAD_CATEGORIES_REQUESTED
        }
    },

    loadCategoriesSuccess: (items) => {
        return {
            type: categoryAction.LOAD_CATEGORIES_SUCCESS,
            payload: {items}
        }
    },

    updateCategoryRequested: (category) => {
        return {
            type: categoryAction.UPDATE_CATEGORY_REQUESTED,
            payload: {category}
        }
    },

    updateCategorySuccess: (category) => {
        return {
            type: categoryAction.UPDATE_CATEGORY_SUCCESS,
            payload: {category}
        }
    },

    deleteCategoryRequested: (category) => {
        return {
            type: categoryAction.DELETE_CATEGORY_REQUESTED,
            payload: {category}
        }
    },

    deleteCategorySuccess: (category) => {
        return {
            type: categoryAction.DELETE_CATEGORY_SUCCESS,
            payload: {category}
        }
    },

    createCategoryRequested: (category) => {
        return {
            type: categoryAction.CREATE_CATEGORY_REQUESTED,
            payload: {category}
        }
    },

    createCategorySuccess: (category) => {
        return {
            type: categoryAction.CREATE_CATEGORY_SUCCESS,
            payload: {category}
        }
    }
};

export default categoryAction;
