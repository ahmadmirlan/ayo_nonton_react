import { combineReducers  } from 'redux';
import Layout from './layout/reducer';
import Auth from './auth/reducer';
import Category from './category/reducer';
import Users from './users/reducer';
import Movies from './movie/reducer';

const rootReducer = combineReducers({
    Layout,
    Auth,
    Category,
    Users,
    Movies
});

export default rootReducer;
