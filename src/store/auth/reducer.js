import authAction from './action';

const initAuthState = {
    isLoading: false,
    user: {},
    isLoginSuccess: false,
    isRegisterSuccess: false,
    isUsernameExist: false,
    isEmailExist: false
};

export default function authReducer(state = initAuthState, {type, payload}) {
    switch (type) {
        case authAction.AUTH_PAGE_ACTION_LOGIN:
            return {
                ...state,
                isLoading: payload.isLoading
            };
        case authAction.LOGIN_SUCCESS:
            return {
                ...state,
                user: payload.user,
                isLoginSuccess: payload.isLoginSuccess
            };
        case authAction.REGISTER_SUCCESS:
            return {
                ...state,
                isRegisterSuccess: payload.isRegisterSuccess
            };
        case authAction.CHECK_USERNAME_SUCCESS:
            return {
                ...state,
                isUsernameExist: payload.isUsernameExist
            };
        case authAction.CHECK_EMAIL_SUCCESS:
            return {
                ...state,
                isEmailExist: payload.isEmailExist
            };
        case authAction.LOGOUT: {
            localStorage.removeItem('yukToken');
            localStorage.removeItem('user');
            return {
                ...state,
                isLoginSuccess: false,
                isRegisterSuccess: false,
                user: {}
            }
        }
        case authAction.CHECK_CURRENT_TOKEN_SUCCESS:
            return {
                ...state,
                isLoginSuccess: true
            };
        default:
            return {
                ...state
            }
    }
}
