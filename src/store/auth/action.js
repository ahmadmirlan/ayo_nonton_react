import authReducer from "./reducer";

const authAction = {
    AUTH_PAGE_ACTION_LOGIN: '[Auth] Auth Page Action Login',
    LOGIN_REQUESTED: '[Login] Login Requested',
    LOGIN_SUCCESS: '[Login] Login Success',
    LOGOUT: '[Logout] Logout Triggered',
    REGISTER_REQUESTED: '[Register] Register Requested',
    REGISTER_SUCCESS: '[Register] Register Success',
    CHECK_USERNAME_REQUESTED: '[Register] Check Username Requested',
    CHECK_USERNAME_SUCCESS: '[Register] Check Username Success',
    CHECK_EMAIL_REQUESTED: '[Register] Check Email Requested',
    CHECK_EMAIL_SUCCESS: '[Register] Check Email Success',
    CHECK_CURRENT_TOKEN_REQUESTED: '[Auth] Check Current Token Requested',
    CHECK_CURRENT_TOKEN_SUCCESS: '[Auth] Check Current Token Success',

    authPageActionLogin: (isLoading) => {
        return {
            type: authAction.AUTH_PAGE_ACTION_LOGIN,
            payload: {isLoading}
        }
    },

    loginRequested: (username, password) => {
        return {
            type: authAction.LOGIN_REQUESTED,
            payload: {username, password}
        }
    },

    loginSuccess: (user, isLoginSuccess) => {
        return {
            type: authAction.LOGIN_SUCCESS,
            payload: {user, isLoginSuccess}
        }
    },

    registerRequested: (firstName, lastName, username, email, password) => {
        return {
            type: authAction.REGISTER_REQUESTED,
            payload: {firstName, lastName, username, email, password}
        }
    },

    registerSuccess: (isRegisterSuccess) => {
        return {
            type: authAction.REGISTER_SUCCESS,
            payload: {isRegisterSuccess}
        }
    },

    logout: () => {
        return {
            type: authAction.LOGOUT
        }
    },

    checkUsernameRequested: (username) => {
        return {
            type: authAction.CHECK_USERNAME_REQUESTED,
            payload: {username}
        }
    },

    checkUsernameSuccess: (isUsernameExist) => {
        return {
            type: authAction.CHECK_USERNAME_SUCCESS,
            payload: {isUsernameExist}
        }
    },

    checkEmailRequested: (email) => {
        return {
            type: authAction.CHECK_EMAIL_REQUESTED,
            payload: {email}
        }
    },

    checkEmailSuccess: (isEmailExist) => {
        return {
            type: authAction.CHECK_EMAIL_SUCCESS,
            payload: {isEmailExist}
        }
    },

    checkCurrentTokenRequested: (token) => {
        return {
            type: authAction.CHECK_CURRENT_TOKEN_REQUESTED,
            payload: {token}
        }
    },

    checkCurrentTokenSuccess: (user) => {
        return {
            type: authAction.CHECK_CURRENT_TOKEN_SUCCESS,
            payload: {user}
        }
    }
};

export default authAction;
