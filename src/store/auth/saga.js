import {all, call, put, takeEvery} from 'redux-saga/effects';
import {loginApi, saveUser, registerApi, checkUsername, checkEmail, findUserByToken} from '../../helpers/auth.helper';
import authAction from './action';
import {message} from 'antd';

function* loginUserSaga({payload: {username, password}}) {
    try {
        yield put(authAction.authPageActionLogin(true));
        const response = yield call(loginApi, username, password);
        message.success('Login success');
        // Save user and token to local storage
        saveUser(response.user);
        yield put(authAction.loginSuccess(response, true));
        yield put(authAction.authPageActionLogin(false));
    } catch (e) {
        message.error('Invalid username or password');
        yield put(authAction.authPageActionLogin(false));
    }
}

function* registerUserSaga({payload: {firstName, lastName, username, email, password}}) {
    try {
        yield put(authAction.authPageActionLogin(true));
        const response = yield call(registerApi, firstName, lastName, username, email, password);
        message.success('Register success');
        // Save user and token to local storage
        saveUser(response.user);
        yield put(authAction.registerSuccess(response));
        yield put(authAction.authPageActionLogin(false));
    } catch (e) {
        message.error('Register failed please try again later!');
        yield put(authAction.authPageActionLogin(false));
    }
}

function* checkUsernameSaga({payload: {username}}) {
    try {
        yield put(authAction.authPageActionLogin(true));
        const response = yield call(checkUsername, username );
        if (response['isExist']) {
            message.warning(`Sorry '${username}' already taken`);
        }
        yield put(authAction.checkUsernameSuccess(response['isExist']));
        yield put(authAction.authPageActionLogin(false));
    } catch (e) {
        yield put(authAction.authPageActionLogin(false));
    }
}

function* checkEmailSaga({payload: {email}}) {
    try {
        yield put(authAction.authPageActionLogin(true));
        const response = yield call(checkEmail, email );
        if (response['isExist']) {
            message.warning(`Sorry '${email}' already taken`);
        }
        yield put(authAction.checkEmailSuccess(response['isExist']));
        yield put(authAction.authPageActionLogin(false));
    } catch (e) {
        yield put(authAction.authPageActionLogin(false));
    }
}

function* checkCurrentTokenWatch({payload}) {
    try {
        yield put(authAction.authPageActionLogin(true));
        const response = yield call(findUserByToken, payload.token);
        yield put(authAction.checkCurrentTokenSuccess(response));
        yield put(authAction.authPageActionLogin(false));
    } catch (e) {
        yield put(authAction.authPageActionLogin(false));
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(authAction.LOGIN_REQUESTED, loginUserSaga),
        takeEvery(authAction.REGISTER_REQUESTED, registerUserSaga),
        takeEvery(authAction.CHECK_USERNAME_REQUESTED, checkUsernameSaga),
        takeEvery(authAction.CHECK_EMAIL_REQUESTED, checkEmailSaga),
        takeEvery(authAction.CHECK_CURRENT_TOKEN_REQUESTED, checkCurrentTokenWatch)
    ])
}
