import {getToken} from '../helpers/auth.helper';
import userAction from './auth/action';

import store from './store';

export default () =>
    new Promise(() => {
        store.dispatch(userAction.checkCurrentTokenRequested(getToken()));
    });
