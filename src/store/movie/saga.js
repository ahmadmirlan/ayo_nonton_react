import {all, call, put, takeEvery} from 'redux-saga/effects';
import movieAction from "./action";
import {findAllMovies, findMovieById, createNewMovie, updateMovie, removeMovie} from "../../helpers/movie.helper";
import {message} from 'antd';

function* findMoviesWatch({payload}) {
    try {
        yield put(movieAction.moviePageToggleLoading(true));
        const response = yield call(findAllMovies, payload.query);
        yield put(movieAction.loadMoviesSuccess(response.data, payload.lastQuery, response.page));
        yield put(movieAction.moviePageToggleLoading(false));
    } catch (e) {
        yield put(movieAction.moviePageToggleLoading(false));
    }
}

function* findMovieByIdWatch({payload}) {
    try {
        yield put(movieAction.moviePageToggleLoading(true));
        const response = yield call(findMovieById, payload.movieId);
        yield put(movieAction.findMovieByIdSuccess(response));
        yield put(movieAction.moviePageToggleLoading(false));
    } catch (e) {
        yield put(movieAction.moviePageToggleLoading(false));
    }
}

function* createMovieWatch({payload}) {
    try {
        yield put(movieAction.moviePageActionLoading(true));
        const response = yield call(createNewMovie, payload.movie);
        yield put(movieAction.createMovieSuccess(response));
        message.success('Create movie success!');
        yield put(movieAction.moviePageActionLoading(false));
        payload.history.push('/dashboard/list/movies');
    } catch (e) {
        message.error('Success movie failed!');
        yield put(movieAction.moviePageActionLoading(false));
    }
}

function* updateMovieWatch({payload}) {
    try {
        yield put(movieAction.moviePageActionLoading(true));
        const response = yield call(updateMovie, payload.movie);
        message.success('Update movie success!');
        yield put(movieAction.updateMovieSuccess(response));
        yield put(movieAction.moviePageActionLoading(false));
        payload.history.push('/dashboard/list/movies');
    } catch (e) {
        yield put(movieAction.moviePageActionLoading(false));
        message.error('Update movie failed!');
    }
}

function* deleteMovieWatch({payload}) {
    try {
        yield put(movieAction.moviePageActionLoading(true));
        yield call(removeMovie, payload.movie.id);
        yield put(movieAction.removeMovieSuccess(payload.movie));
        message.success(`"${payload.movie.title}" deleted!`);
        yield put(movieAction.moviePageActionLoading(false));
    } catch (e) {
        yield put(movieAction.moviePageActionLoading(false));
        message.error(`Failed deleting a movie!`);
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(movieAction.LOAD_MOVIES_REQUESTED, findMoviesWatch),
        takeEvery(movieAction.FIND_MOVIE_BY_ID_REQUESTED, findMovieByIdWatch),
        takeEvery(movieAction.CREATE_MOVIE_REQUESTED, createMovieWatch),
        takeEvery(movieAction.UPDATE_MOVIE_REQUESTED, updateMovieWatch),
        takeEvery(movieAction.REMOVE_MOVIE_REQUESTED, deleteMovieWatch)
    ])
}
