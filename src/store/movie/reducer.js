import movieAction from "./action";
import {produce} from 'immer';

const initMovieState = {
    movies: [],
    movie: null,
    lastCreatedMovie: null,
    lastRemovedMovie: null,
    lastQuery: {},
    isLoading: false,
    actionLoading: false,
    pagesInfo: {},
    search: '',
};

export default function movieReducer(state = initMovieState, {type, payload}) {
    switch (type) {
        case movieAction.MOVIE_PAGE_TOGGLE_LOADING:
            return {
                ...state,
                isLoading: payload.isLoading
            };
        case movieAction.MOVIE_PAGE_ACTION_LOADING:
            return {
                ...state,
                actionLoading: payload.isLoading
            };
        case movieAction.LOAD_MOVIES_SUCCESS:
            return {
                ...state,
                movies: payload.items,
                lastQuery: payload.lastQuery,
                pagesInfo: payload.pagesInfo
            };
        case movieAction.MOVIE_LOCAL_QUERY_SEARCH:
            return {
                ...state,
                search: payload.search
            };
        case movieAction.FIND_MOVIE_BY_ID_SUCCESS:
            return {
                ...state,
                movie: payload.movie
            };
        case movieAction.CREATE_MOVIE_SUCCESS:
            const newListMovie = produce(state.movies, draftState => {
                draftState.push(payload.movie);
            });
            return {
                ...state,
                movies: newListMovie,
                lastCreatedMovie: payload.movie
            };
        case movieAction.CALL_EDIT_MOVIE_PAGE:
            return {
                ...state,
                movie: null
            };
        case movieAction.UPDATE_MOVIE_SUCCESS:
            return {
                ...state,
                lastCreatedMovie: payload.movie
            };
        case movieAction.REMOVE_MOVIE_SUCCESS:
            let movieList = produce(state.movies, draftState => {
            });
            movieList = movieList.filter(movie => movie.id !== payload.movie.id);
            return {
                ...state,
                movies: movieList,
                lastRemovedMovie: payload.movie
            };
        default:
            return {
                ...state
            }
    }
}
