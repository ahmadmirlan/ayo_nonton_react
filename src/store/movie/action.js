const movieAction = {
    MOVIE_PAGE_TOGGLE_LOADING: '[Movie] Movie Page Toggle Loading',
    MOVIE_PAGE_ACTION_LOADING: '[Movie] Movie Page Action Loading',
    LOAD_MOVIES_REQUESTED: '[Movie] Load Movies Requested',
    LOAD_MOVIES_SUCCESS: '[Movie] Load Movies Success',
    MOVIE_LOCAL_QUERY_SEARCH: '[Search] Movie Local Query Search',
    FIND_MOVIE_BY_ID_REQUESTED: '[Movie] Find Movie By Id Requested',
    FIND_MOVIE_BY_ID_SUCCESS: '[Movie] Find Movie By Id Success',
    CALL_EDIT_MOVIE_PAGE: '[Movie] Call Edit Movie Page',
    CREATE_MOVIE_REQUESTED: '[Movie] Create Movie Requested',
    CREATE_MOVIE_SUCCESS: '[Movie] Create Movie Success',
    UPDATE_MOVIE_REQUESTED: '[Movie] Update Movie Requested',
    UPDATE_MOVIE_SUCCESS: '[Movie] Update Movie Success',
    REMOVE_MOVIE_REQUESTED: '[Movie] Remove Movie Requested',
    REMOVE_MOVIE_SUCCESS: '[Movie] Remove Movie Success',

    moviePageToggleLoading: (isLoading) => {
        return {
            type: movieAction.MOVIE_PAGE_TOGGLE_LOADING,
            payload: {isLoading}
        }
    },

    moviePageActionLoading: (isLoading) => {
        return {
            type: movieAction.MOVIE_PAGE_ACTION_LOADING,
            payload: {isLoading}
        }
    },

    loadMoviesRequested: (query) => {
        return {
            type: movieAction.LOAD_MOVIES_REQUESTED,
            payload: {query}
        }
    },

    loadMoviesSuccess: (items, lastQuery, pagesInfo) => {
        return {
            type: movieAction.LOAD_MOVIES_SUCCESS,
            payload: {items, lastQuery, pagesInfo}
        }
    },

    movieLocalQuerySearch: (search) => {
        return {
            type: movieAction.MOVIE_LOCAL_QUERY_SEARCH,
            payload: {search}
        }
    },

    findMovieByIdRequested: (movieId) => {
        return {
            type: movieAction.FIND_MOVIE_BY_ID_REQUESTED,
            payload: {movieId}
        }
    },

    findMovieByIdSuccess: (movie) => {
        return {
            type: movieAction.FIND_MOVIE_BY_ID_SUCCESS,
            payload: {movie}
        }
    },

    createMovieRequested: (movie, history) => {
        return {
            type: movieAction.CREATE_MOVIE_REQUESTED,
            payload: {movie, history}
        }
    },

    createMovieSuccess: (movie) => {
        return {
            type: movieAction.CREATE_MOVIE_SUCCESS,
            payload: {movie}
        }
    },

    callEditMoviePage: () => {
        return {
            type: movieAction.CALL_EDIT_MOVIE_PAGE
        }
    },

    updateMovieRequested: (movie, history) => {
        return {
            type: movieAction.UPDATE_MOVIE_REQUESTED,
            payload: {movie, history}
        }
    },

    updateMovieSuccess: (movie) => {
        return {
            type: movieAction.UPDATE_MOVIE_SUCCESS,
            payload: {movie}
        }
    },

    removeMovieRequested: (movie) => {
        return {
            type: movieAction.REMOVE_MOVIE_REQUESTED,
            payload: {movie}
        }
    },

    removeMovieSuccess: (movie) => {
        return {
            type: movieAction.REMOVE_MOVIE_SUCCESS,
            payload: {movie}
        }
    }
};

export default movieAction;
