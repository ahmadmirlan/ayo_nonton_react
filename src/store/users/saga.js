import {all, call, put, takeEvery} from 'redux-saga/effects';
import userAction from './action';
import {findAllUsers} from '../../helpers/user.helper';

function* findUserWatch({payload}) {
    try {
        yield put(userAction.userPageToggleLoading(true));
        const response = yield call(findAllUsers, payload.query);
        yield put(userAction.loadUsersSuccess(response.data, payload.query, response.page));
        yield put(userAction.userPageToggleLoading(false));
    } catch (e) {
        yield put(userAction.userPageToggleLoading(false));
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(userAction.LOAD_USERS_REQUESTED, findUserWatch)
    ])
}
