const userAction = {
    USER_PAGE_TOGGLE_LOADING: '[User] User Page Toggle Loading',
    USER_PAGE_ACTION_LOADING: '[User] User Page Action Loading',
    LOAD_USERS_REQUESTED: '[User] Load Users Requested',
    LOAD_USERS_SUCCESS: '[User] Load Users Success',

    userPageToggleLoading: (isLoading) => {
        return {
            type: userAction.USER_PAGE_TOGGLE_LOADING,
            payload: {isLoading}
        }
    },

    userPageActionLoading: (isLoading) => {
        return {
            type: userAction.USER_PAGE_ACTION_LOADING,
            payload: {isLoading}
        }
    },

    loadUsersRequested: (query) => {
        return {
            type: userAction.LOAD_USERS_REQUESTED,
            payload: {query}
        }
    },

    loadUsersSuccess: (items, lastQuery, pagesInfo) => {
        return {
            type: userAction.LOAD_USERS_SUCCESS,
            payload: {items, lastQuery, pagesInfo}
        }
    }
};

export default userAction;
