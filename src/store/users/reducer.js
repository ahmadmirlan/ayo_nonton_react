import userAction from "../users/action";

const initUserState = {
    users: [],
    lastQuery: {},
    isLoading: false,
    pagesInfo: {}
};

export default function userReducer(state = initUserState, {type, payload}) {
    switch (type) {
        case userAction.USER_PAGE_ACTION_LOADING: {
            return {
                ...state,
                isLoading: payload.isLoading
            };
        }
        case userAction.USER_PAGE_TOGGLE_LOADING: {
            return {
                ...state,
                isLoading: payload.isLoading
            }
        }
        case userAction.LOAD_USERS_SUCCESS: {
            return {
                ...state,
                users: payload.items,
                lastQuery: payload.lastQuery,
                pagesInfo: payload.pagesInfo
            };
        }
        default:
            return {
                ...state
            }
    }
}
