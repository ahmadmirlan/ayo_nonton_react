import i18n from "i18next";
import {initReactI18next} from "react-i18next";
import translationEN from './assets/i18n/en';
import translationID from './assets/i18n/id';

export default function Translation(lang) {
    i18n
        .use(initReactI18next) // passes i18n down to react-i18next
        .init({
            resources: {
                en: {
                    translation: translationEN
                },
                id: {
                    translation: translationID
                }
            },
            lng: lang ? lang : 'en',
            fallbackLng: "en",

            interpolation: {
                escapeValue: false
            }
        });
}
