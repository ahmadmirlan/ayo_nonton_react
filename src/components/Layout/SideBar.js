import React from 'react';
import {useSelector} from 'react-redux';
import {useHistory} from "react-router-dom";
import {Icon} from 'antd';
import logo from '../../assets/images/logo192.png';

import clip from '../../assets/images/clip.jpg';

export default function SideBar() {
    const {isCollapsed} = useSelector(state => state.Layout);
    const history = useHistory();

    const changePages = (pageUrl) => {
        history.push(pageUrl);
    };

    const isActive = (pageUrl) => {
        return history.location.pathname.includes(pageUrl);
    };

    return (
        <div className="sidebar">
            <div className="sidebar__header">
                <div className="sidebar__header--title">
                    <img src={logo} alt="Logo" className="icon-title"/>
                    <div className="text-title">Yuk Nonton</div>
                </div>
                <div className="sidebar__header--profile">
                    <img src={clip} alt="Cover" className="mx-auto"/>
                    <p className="sidebar__header--profile-name">Ahmad Mirlan</p>
                    <p className="sidebar__header--profile-username">&#64;ahmadmirlan</p>
                </div>
            </div>
            <div className="sidebar__nav">
                <span className={`sidebar__nav--item ${isActive('/dashboard/home') ? 'active' : ''}`}
                      onClick={() => changePages('/dashboard/home')}>
                    <Icon type="home"/>
                    <p className="item-text">Home</p>
                </span>
                <span className={`sidebar__nav--item ${isActive('/dashboard/movies') ? 'active' : ''}`}
                      onClick={() => changePages('/dashboard/movies')}>
                    <Icon type="video-camera"/>
                    <p className="item-text">Videos</p>
                </span>
                <span className={`sidebar__nav--item ${isActive('/dashboard/categories') ? 'active' : ''}`}
                      onClick={() => changePages('/dashboard/categories')}>
                    <Icon type="bars"/>
                    <p className="item-text">Categories</p>
                </span>
                <span className={`sidebar__nav--item ${isActive('/dashboard/list/movies') ? 'active' : ''}`}
                      onClick={() => changePages('/dashboard/list/movies')}>
                    <Icon type="table"/>
                    <p className="item-text">Movie Table</p>
                </span>
                <span className={`sidebar__nav--item ${isActive('/dashboard/users') ? 'active' : ''}`}
                      onClick={() => changePages('/dashboard/users')}>
                    <Icon type="team"/>
                    <p className="item-text">Users</p>
                </span>
            </div>
        </div>
    )
}
