import React from 'react';
import SideBar from "./SideBar";
import TopBar from "./TopBar";
import {useSelector} from "react-redux";
import {Modal} from "antd";
import {useHistory} from 'react-router';

export default function LayoutWrapper(props) {
    const {isLoginSuccess, isLoading} = useSelector(state => state.Auth);
    const history = useHistory();

    const redirectToPageLogin = () => {
        history.push('/auth/login')
    };

    const showAlert = () => {
        Modal.warning({
            title: 'You have to login to use the app',
            content: `Currently you're not logged in or session already expired`,
            onOk: () => redirectToPageLogin(),
            okText: 'Login'
        })
    };

    return (
        <React.Fragment>
            {isLoginSuccess &&
            <div className="main">
                <SideBar/>
                <div className="main-wrapper">
                    <TopBar/>
                    {props.children}
                </div>
            </div>
            }
            {!isLoginSuccess && !isLoading && showAlert()}
        </React.Fragment>
    );
}
