import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Icon,  Menu, Input} from 'antd';
import layoutAction from '../../store/layout/action';
import {useHistory} from 'react-router';
import authAction from "../../store/auth/action";
import clip from '../../assets/images/clip.jpg';
import movieAction from "../../store/movie/action";
const { Search } = Input;

function TopBar() {
    const {isCollapsed} = useSelector(state => state.Layout);
    const dispatch = useDispatch();
    const history = useHistory();

    const searchMovie = (value) => {
        dispatch(movieAction.movieLocalQuerySearch(value));
        history.push(`/dashboard/movies`);
    };

    const logoutUser = () => {
        dispatch(authAction.logout());
        history.push('/auth/login');
    };

    const menu = (
        <Menu>
            <Menu.Item key="0">
                <a href="http://www.alipay.com/">
                    <Icon type="menu-fold"/>&nbsp; &nbsp;Profile &nbsp; &nbsp;</a>
            </Menu.Item>
            <Menu.Divider />
            <Menu.Item key="3" onClick={logoutUser}><Icon type="menu-fold"/>    Logout</Menu.Item>
        </Menu>
    );

    const toggle = () => {
        dispatch(layoutAction.sideBarCollapsed(!isCollapsed));
    };

    return (
        <div className="topbar">
            <Search placeholder="Search" onSearch={value => searchMovie(value)}
                    className="topbar__search"/>
            <div className="topbar__info">
                <img src={clip} alt="Cover" className="topbar__info--image" />
            </div>
        </div>
    )
}

export default TopBar;
