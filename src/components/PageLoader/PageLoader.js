import React from "react";
import {Spin} from 'antd';

export default function PageLoader() {
    return (
        <div className="body-loader">
            <Spin size={"large"}/>
            <p className="body-loader__text">
                Saving...
            </p>
        </div>
    )
}
