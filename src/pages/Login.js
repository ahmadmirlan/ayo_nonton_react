import React, {useEffect} from 'react';
import BgLogin from '../assets/images/pages/bg-login.jpg';
import {Checkbox, Icon} from 'antd';
import {useForm} from 'react-hook-form'
import {useDispatch, useSelector} from 'react-redux'
import authAction from "../store/auth/action";
import {Link} from 'react-router-dom';
import {useHistory} from 'react-router';
import { useTranslation } from "react-i18next";
function Login() {

    const {isLoading, isLoginSuccess} = useSelector(state => state.Auth);
    const dispatch = useDispatch();
    const history = useHistory();
    const {t} = useTranslation();
    const {handleSubmit, register, errors} = useForm();

    const onLogin = values => {
        dispatch(authAction.loginRequested(values.username, values.password));
    };

    // Life cycle
    useEffect(() => {
        if (isLoginSuccess) {
            history.push('/dashboard');
        }
    },[isLoginSuccess, history]);

    return (
        <React.Fragment>
            <div className="login-page">
                <div className="login-card">
                    <div className="login-card__left">
                        <div className="login">
                            <p className="login__title">
                          <span className="login__title--main">
                              {t('LOGIN.WELCOME_MESSAGE')}
                          </span>
                                <span className="login__title--sub">
                              Let's Explore
                          </span>
                            </p>
                            <div className="login__form">
                                <input placeholder="username" name="username"
                                       className={`login__form--input ${errors.username ? 'error-input' : ''}`}
                                       ref={register({
                                           validate: value => value.length > 5
                                       })}/>
                                <input type="password" placeholder="password" name="password"
                                       className={`login__form--input ${errors.password ? 'error-input' : ''}`}
                                       ref={register({
                                           validate: value => value.length > 2
                                       })}/>
                                <div className="login-info">
                                    <div className="remember">
                                        <Checkbox/> Remember me
                                    </div>
                                    <a href="#" className="forgot">Forgot Password?</a>
                                </div>
                            </div>
                            <div className="login__action">
                                <button className="login__action--button" onClick={handleSubmit(onLogin)}
                                        disabled={isLoading}>
                                    {isLoading && <Icon type="loading"/>} Login
                                </button>
                            </div>
                            <div className="login__message">
                                <p>Don't have an account? <Link to="/auth/register">Register Here</Link></p>
                            </div>
                        </div>
                    </div>
                    <div className="login-card__right">
                        <img src={BgLogin} alt="Login Background" className="card__right--image"/>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Login;
