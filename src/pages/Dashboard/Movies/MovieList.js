import React, {useEffect, useState} from "react";
import {useSelector, useDispatch} from 'react-redux';
import movieAction from "../../../store/movie/action";
import {Icon, Popconfirm, Spin, Table, Tag} from "antd";
import {useHistory} from 'react-router';

export default function MovieList() {
    const [query, setQuery] = useState({
        filter: {},
        pageNumber: "0",
        pageSize: 10,
        sortField: "createdAt",
        sortOrder: "desc"
    });

    const {isLoading, movies} = useSelector(state => state.Movies);
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        dispatch(movieAction.loadMoviesRequested(query));
    }, [dispatch, query]);

    // On edit movie
    const onEditMovie = (movieId) => {
        dispatch(movieAction.callEditMoviePage());
        history.push(`/dashboard/movies/edit/${movieId}`);
    };

    const onDeleteMovie = (movie) => {
      dispatch(movieAction.removeMovieRequested(movie));
    };

    // Table columns
    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'firstName',
            width: '150px',
            render: (text, row) => {
                return (<p>{row.title}</p>);
            },
        },
        {
            title: 'Categories',
            dataIndex: 'categories',
            key: 'categories',
            width: '150px',
            render: (text, row) => {
                return (
                    <React.Fragment>
                        {row.categories.map((category, idx) => {
                            return <Tag color="gold" key={idx}>{category.name}</Tag>
                        })}
                    </React.Fragment>
                );
            },
        },
        {
            title: 'Actors',
            dataIndex: 'actors',
            key: 'actors',
            width: '150px',
            render: (text, row) => {
                return <React.Fragment>
                    {row.actors.map((actor, idx) => <Tag color="geekblue" key={idx}>{actor}</Tag>)}
                </React.Fragment>;
            },
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'email',
            width: '150px',
            render: (text, row) => {
                return (<Tag color="lime">{row.status}</Tag>);
            },
        },
        {
            title: 'Actions',
            key: 'action',
            width: '100px',
            className: 'noWrapCell',
            render: (text, row) => {
                return (
                    <div className="action-wrapper">
                        <button className="button button__icon" onClick={() => onEditMovie(row.id)}>
                            <Icon type="edit"/>
                        </button>
                        <Popconfirm title="Are you sure to delete this movie？"
                                    okText="Yes"
                                    cancelText="No"
                                    onConfirm={() => onDeleteMovie(row)}
                                    placement="topRight">
                            <button className="button button__icon--danger">
                                <Icon type="delete"/>
                            </button>
                        </Popconfirm>
                    </div>
                );
            },
        },
    ];

    return (
        <div className="page-content">
            <div className="page-content__title">
                <div className="page-content__title--text">
                    Movie Tables
                </div>
                <div className="page-content__title--action">
                    <button
                        onClick={() => {
                            dispatch(movieAction.callEditMoviePage());
                            return history.push('/dashboard/movies/create')
                        }}
                        className="bg-pink-500 text-white font-bold py-2 px-4 rounded inline-flex items-center">
                        <svg viewBox="64 64 896 896" focusable="false" className="fill-current w-6 h-6 mr-2"
                             data-icon="file-add" width="1em"
                             height="1em" fill="currentColor" aria-hidden="true">
                            <path
                                d="M854.6 288.6L639.4 73.4c-6-6-14.1-9.4-22.6-9.4H192c-17.7 0-32 14.3-32 32v832c0 17.7 14.3 32 32 32h640c17.7 0 32-14.3 32-32V311.3c0-8.5-3.4-16.7-9.4-22.7zM790.2 326H602V137.8L790.2 326zm1.8 562H232V136h302v216a42 42 0 0 0 42 42h216v494zM544 472c0-4.4-3.6-8-8-8h-48c-4.4 0-8 3.6-8 8v108H372c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8h108v108c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V644h108c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8H544V472z"/>
                        </svg>
                        <span>New</span>
                    </button>
                    {/*<button className="btn-action" onClick={() => history.push('/dashboard/movies/create')}><Icon type="file-add"/> New</button>*/}
                </div>
            </div>
            <div className="page-content__body">
                {!isLoading &&
                <div className="ant-table-responsive">
                    <Table dataSource={movies} columns={columns} loading={isLoading} rowKey="id"
                           pagination={{
                               defaultPageSize: 5,
                               showSizeChanger: true,
                               defaultCurrent: 0,
                               pageSizeOptions: ['3', '5', '10'],
                           }}
                    />
                </div>
                }
                {isLoading &&
                <div className="body-loader">
                    <Spin size="large"/>
                </div>
                }
            </div>
        </div>
    )
}
