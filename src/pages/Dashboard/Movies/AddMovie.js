import React, {useEffect} from "react";
import categoryAction from "../../../store/category/action";
import {DatePicker, Input, InputNumber, Select} from "antd";
import {useDispatch, useSelector} from 'react-redux';
import * as Yup from 'yup';
import {Field, Form, Formik} from 'formik';
import ReactQuill from "react-quill";
import moment from 'moment';
import {useHistory, useParams} from 'react-router';
import movieAction from "../../../store/movie/action";
import PageLoader from "../../../components/PageLoader/PageLoader";

const {Option} = Select;

function AddMovie() {

    const {categories} = useSelector(state => state.Category);
    const {isLoading, movie, actionLoading, lastCreatedMovie} = useSelector(state => state.Movies);
    const dispatch = useDispatch();
    const history = useHistory();
    const {movieId} = useParams();

    useEffect(() => {
        if (movieId) {
            dispatch(movieAction.findMovieByIdRequested(movieId));
        }
        if (categories.length < 1) {
            dispatch(categoryAction.loadCategoriesRequested());
        }
    }, [dispatch, categories, movieId]);

    const onSaveMovie = (movie) => {
        // update else will create
        if (movieId) {
            movie.id = movieId;
            dispatch(movieAction.updateMovieRequested(movie, history));
        } else {
            dispatch(movieAction.createMovieRequested(movie, history));
        }
    };

    /* Category Option */
    const categoryOption = [];
    if (categories) {
        categories.forEach(category => {
            categoryOption.push(<Option key={category.id}>{category.name}</Option>);
        });
    }

    /*Status Option*/
    const statusOption = [];
    statusOption.push(<Option key="AVAILABLE">{'Available'}</Option>);
    statusOption.push(<Option key="UP_COMING">{'Up Coming'}</Option>);
    statusOption.push(<Option key="NOT_AVAILABLE">{'Not Available'}</Option>);

    /* Form rule validation */
    const MovieSchema = Yup.object().shape({
        title: Yup.string()
            .max(50, 'Title too long!')
            .required('Please provide title'),
        cover: Yup.string()
            .required('Please provide movie cover'),
        actors: Yup.array(),
        categories: Yup.array()
            .required('Please choose category'),
        trailerURL: Yup.string()
            .required('Please provide trailer url'),
        streamURL: Yup.string()
            .required('Please provide stream url'),
        rentPrices: Yup.number()
            .required('Please provide rent prices'),
        releaseDate: Yup.string()
            .required('Please provide date'),
        status: Yup.string().required('Please choose status'),
        description: Yup.string().required('Please provide movie description')
    });

    return (
        <div className="page-content">
            <div className="page-content__title">
                <div className="page-content__title--text">
                    {movieId ? 'Update Movie' : 'Add New Movie'}
                </div>
            </div>
            <div className="page-content__body">
                {!isLoading && !actionLoading && <Formik
                    initialValues={{
                        title: movie && movie.title ? movie.title : '',
                        cover: movie && movie.cover ? movie.cover : '',
                        actors: movie && movie.actors ? movie.actors : [],
                        categories: movie && movie.categories ? movie.categories : [],
                        trailerURL: movie && movie.trailerURL ? movie.trailerURL : '',
                        streamURL: movie && movie.streamURL ? movie.streamURL : '',
                        rentPrices: movie && movie.rentPrices ? movie.rentPrices : '',
                        releaseDate: movie && movie.releaseDate ? movie.releaseDate : '',
                        status: movie && movie.status ? movie.status : '',
                        description: movie && movie.description ? movie.description : ''
                    }}
                    validationSchema={MovieSchema}
                    onSubmit={values => {
                        // same shape as initial values
                        onSaveMovie(values);
                    }}
                >
                    {({errors, touched, values, setFieldTouched, setFieldValue}) => (
                        <Form>
                            <div className="flex flex-wrap -mx-3">
                                <div className="w-full md:w-1/2 px-3 mb-6">
                                    <Field name="title" render={({field}) => <Input {...field} placeholder="Title"/>}/>
                                    {errors.title && touched.title &&
                                    <span className="input-label-error">{errors.title}</span>}
                                </div>
                                <div className="w-full md:w-1/2 px-3 mb-6">
                                    <Field name="cover" render={({field}) => <Input {...field} placeholder="Cover"/>}/>
                                    {errors.cover && touched.cover &&
                                    <span className="input-label-error">{errors.cover}</span>}
                                </div>
                                <div className="w-full md:w-1/2 px-3 mb-6">
                                    <Field name="actors" render={({field}) =>
                                        <Select {...field} mode="tags" style={{width: '100%'}}
                                                onChange={value => setFieldValue("actors", value)}
                                                onBlur={() => setFieldTouched("actors", true)}
                                                value={values.actors}
                                                tokenSeparators={[',']} placeholder="Actors"/>}/>
                                </div>
                                <div className="w-full md:w-1/2 px-3 mb-6">
                                    <Field name="categories" render={({field}) =>
                                        <Select
                                            {...field}
                                            mode="multiple"
                                            style={{width: '100%'}}
                                            placeholder="Categories"
                                            onChange={value => setFieldValue("categories", value)}
                                            onBlur={() => setFieldTouched("categories", true)}
                                            value={values.categories}
                                        >
                                            {categoryOption}
                                        </Select>}/>
                                    {errors.categories && touched.categories &&
                                    <span className="input-label-error">{errors.categories}</span>}
                                </div>
                                <div className="w-full md:w-1/2 px-3 mb-6">
                                    <Field name="trailerURL" render={({field}) =>
                                        <Input {...field} placeholder="Trailer Url"/>
                                    }/>
                                    {errors.trailerURL && touched.trailerURL &&
                                    <span className="input-label-error">{errors.trailerURL}</span>}
                                </div>
                                <div className="w-full md:w-1/2 px-3 mb-6">
                                    <Field name="streamURL" render={({field}) =>
                                        <Input {...field} placeholder="Stream Url"/>
                                    }/>
                                    {errors.streamURL && touched.streamURL &&
                                    <span className="input-label-error">{errors.streamURL}</span>}
                                </div>
                                <div className="w-full md:w-1/2 px-3 mb-6">
                                    <Field name="rentPrices" render={({field}) =>
                                        <InputNumber {...field} placeholder="Rent Prices"
                                                     onChange={value => setFieldValue("rentPrices", value)}
                                                     onBlur={() => setFieldTouched("rentPrices", true)}
                                                     value={values.rentPrices}
                                                     style={{'width': '100%'}} min={0}/>
                                    }/>
                                    {errors.rentPrices && touched.rentPrices &&
                                    <span className="input-label-error">{errors.rentPrices}</span>}
                                </div>
                                <div className="w-full md:w-1/4 px-3 mb-6">
                                    <Field name="releaseDate" render={({field}) =>
                                        <DatePicker {...field} style={{'width': '100%'}}
                                                    onChange={(_, dateString) => setFieldValue('releaseDate', dateString)}
                                                    value={values.releaseDate ? (moment(values.releaseDate, 'YYYY-MM-DD')) : null}/>
                                    }/>
                                    {errors.releaseDate && touched.releaseDate &&
                                    <span className="input-label-error">{errors.releaseDate}</span>}
                                </div>
                                <div className="w-full md:w-1/4 px-3 mb-6">
                                    <Field name="status" render={({field}) =>
                                        <Select
                                            {...field}
                                            style={{width: '100%'}}
                                            placeholder="Status"
                                            onChange={value => setFieldValue("status", value)}
                                            onBlur={() => setFieldTouched("status", true)}
                                            value={values.status}
                                        >
                                            {statusOption}
                                        </Select>
                                    }/>
                                    {errors.status && touched.status &&
                                    <span className="input-label-error">{errors.status}</span>}
                                </div>
                                <div className="w-full px-3 mb-6">
                                    <Field name="description" render={({field}) =>
                                        <ReactQuill {...field} value={values.description}
                                                    onBlur={() => setFieldTouched("description", true)}
                                                    onChange={value => setFieldValue("description", value)}/>
                                    }/>
                                </div>
                                <div className="w-full md:w-1/4 px-3 mb-6">
                                    <div className="flex -row-flex-end">
                                        <button
                                            type="button"
                                            onClick={() => history.push('/dashboard/movies')}
                                            className="bg-pink-500 text-white font-semibold py-2 px-4 border border-gray-400 rounded shadow mr-3">
                                            Cancel
                                        </button>
                                        <button
                                            type="submit"
                                            disabled={Object.keys(errors).length > 0}
                                            className={`bg-blue-500 text-white font-semibold py-2 px-4 border border-gray-400 rounded shadow
                                            ${Object.keys(errors).length > 0 ? 'opacity-50 cursor-not-allowed' : ''}`}>
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>}
                {actionLoading && <PageLoader/>}
            </div>
        </div>
    )
}

export default AddMovie;
