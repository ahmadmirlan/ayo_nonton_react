import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from 'react-redux';
import {Col, Icon, Row, Spin, Tag} from "antd";
import movieAction from "../../../store/movie/action";
import {useHistory} from 'react-router';

export default function Movies() {

    const {isLoading, movies, pagesInfo, search} = useSelector(state => state.Movies);
    const dispatch = useDispatch();
    const history = useHistory();

    const [query, setQuery] = useState({
        filter: {
            title: ''
        },
        pageNumber: "0",
        pageSize: 10,
        sortField: "createdAt",
        sortOrder: "desc"
    });

    useEffect(() => {
        const prevQuery = query;
        prevQuery.filter.title = search;
        dispatch(movieAction.loadMoviesRequested(prevQuery));
    }, [dispatch, search, query]);

    // On create new movie
    const onCreateNewMovie = () => {
        dispatch(movieAction.callEditMoviePage());
        history.push('/dashboard/movies/create')
    };

    return (
        <div className="page-content">
            <div className="page-content__title">
                <div className="page-content__title--text">
                    Movies
                </div>
                <div className="page-content__title--action">
                    <button
                        onClick={onCreateNewMovie}
                        className="bg-pink-500 text-white font-bold py-2 px-4 rounded inline-flex items-center">
                        <svg viewBox="64 64 896 896" focusable="false" className="fill-current w-6 h-6 mr-2" data-icon="file-add" width="1em"
                             height="1em" fill="currentColor" aria-hidden="true">
                            <path d="M854.6 288.6L639.4 73.4c-6-6-14.1-9.4-22.6-9.4H192c-17.7 0-32 14.3-32 32v832c0 17.7 14.3 32 32 32h640c17.7 0 32-14.3 32-32V311.3c0-8.5-3.4-16.7-9.4-22.7zM790.2 326H602V137.8L790.2 326zm1.8 562H232V136h302v216a42 42 0 0 0 42 42h216v494zM544 472c0-4.4-3.6-8-8-8h-48c-4.4 0-8 3.6-8 8v108H372c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8h108v108c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V644h108c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8H544V472z"/>
                        </svg>
                        <span>New</span>
                    </button>
                    {/*<button className="btn-action" onClick={() => history.push('/dashboard/movies/create')}><Icon type="file-add"/> New</button>*/}
                </div>
            </div>
            <div className="page-content__body">
                {!isLoading &&
                <Row gutter={[16, 48]}>
                    {movies.map((movie, idx) => {
                        return (
                            <Col xl={6} lg={8} md={12} sm={24} xs={24} key={idx}>
                                <div className="card">
                                    <img src={movie.cover} alt={movie.title}/>
                                </div>
                                <div className="movie-bottom-title">
                                    <span className="movie-bottom-title__main">{movie.title}</span>
                                    <span className="movie-bottom-title__sub">
                                        {movie.categories.map((category, index) => {
                                            return (
                                                <Tag color="cyan" key={index}>{category.name}</Tag>
                                            )
                                        })}
                                    </span>
                                </div>
                            </Col>
                        )
                    })}
                </Row>
                }
                {isLoading &&
                <div className="body-loader">
                    <Spin size="large"/>
                </div>
                }
            </div>
        </div>
    )
}
