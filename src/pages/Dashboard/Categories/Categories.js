import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Icon, Input, Modal, Popconfirm, Table, Tag} from 'antd';
import categoryAction from '../../../store/category/action';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import PageLoader from "../../../components/PageLoader/PageLoader";

function Categories() {

    const [modal, setModal] = useState(false);
    const [category, setCategory] = useState({});

    const {isLoading, categories, actionLoading} = useSelector(state => state.Category);
    const dispatch = useDispatch();

    // Define category validation
    const CATEGORY_SCHEMA = Yup.object().shape({
        name: Yup.string()
            .min(2, 'At least 2 characters')
            .max(15, 'Name too long')
            .required('Please provide category name')
    });

    // Define form
    let categoryForm = useFormik({
        validationSchema: CATEGORY_SCHEMA,
        initialValues: {
            name: '',
        },
        onSubmit: values => {
            if (category && category.id) {
                dispatch(categoryAction.updateCategoryRequested({...values, id: category.id}));
            } else {
                dispatch(categoryAction.createCategoryRequested(values));
            }
            hideModal();
        },
    });

    useEffect(() => {
        dispatch(categoryAction.loadCategoriesRequested());
    }, [dispatch]);

    // Hide modal
    const hideModal = () => {
        setModal(false);
        setCategory({});
    };

    // Show modal
    const showModal = () => {
        setModal(true);
        setCategory({});
    };

    // Edit modal
    const editModal = async (category) => {
        await setCategory(category);
        setModal(true);
        categoryForm.initialValues.name = category && category.name ? category.name : '';
    };

    // On delete categories
    const deleteCategory = (category) => {
        dispatch(categoryAction.deleteCategoryRequested(category));
    };

    // Table columns
    const columns = [
        {
            title: 'Category Name',
            dataIndex: 'name',
            key: 'name',
            width: '150px',
            render: (text, row) => {
                return (<Tag color="green">{row.name}</Tag>);
            },
        },
        {
            title: 'Actions',
            key: 'action',
            width: '100px',
            className: 'noWrapCell',
            render: (text, row) => {
                return (
                    <div className="action-wrapper">
                        <button className="button button__icon" onClick={() => editModal(row)}>
                            <Icon type="edit"/>
                        </button>
                        <Popconfirm title="Are you sure to delete this category？"
                                    okText="Yes"
                                    cancelText="No"
                                    onConfirm={() => deleteCategory(row)}
                                    placement="topRight">
                            <button className="button button__icon--danger">
                                <Icon type="delete"/>
                            </button>
                        </Popconfirm>
                    </div>
                );
            },
        },
    ];

    // Return html
    return (
        <React.Fragment>
            {!actionLoading && <div className="page-content">
                <div className="page-content__title">
                    <div className="page-content__title--text">
                        Categories
                    </div>
                    <div className="page-content__title--action">
                        <button className="btn-action" onClick={showModal}><Icon type="file-add"/> New</button>
                    </div>
                </div>
                <div className="page-content__body">
                    <div className="ant-table-responsive">
                        <Table dataSource={categories} columns={columns} loading={isLoading} rowKey="id"
                               pagination={{
                                   defaultPageSize: 5,
                                   showSizeChanger: true,
                                   defaultCurrent: 0,
                                   pageSizeOptions: ['3', '5', '10'],
                               }}
                        />
                    </div>
                </div>
            </div>}
            {
                actionLoading && <PageLoader/>
            }
            {
                modal && <Modal
                    title={category.id ? 'Update Category' : 'Add Category'}
                    visible={modal}
                    onCancel={hideModal}
                    onOk={categoryForm.handleSubmit}
                >
                    <div className="input-group">
                        <span className="input-label">Category Name</span>
                        <Input placeholder="category name" name="name" defaultValue={category.name}
                               onChange={categoryForm.handleChange}
                        />
                        {categoryForm.errors.name && categoryForm.touched.name &&
                        <span className="input-label-error">{categoryForm.errors.name}</span>}
                    </div>
                </Modal>
            }
        </React.Fragment>
    )
}

export default Categories;
