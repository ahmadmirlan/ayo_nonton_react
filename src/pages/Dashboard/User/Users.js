import React, {useEffect, useState} from "react";
import {Icon, Popconfirm, Table, Tag} from "antd";
import {useSelector, useDispatch} from 'react-redux';
import userAction from "../../../store/users/action";
export default function Users() {

    const [query, setQuery] = useState({
        filter: {},
        pageNumber: "0",
        pageSize: 10,
        sortField: "createdAt",
        sortOrder: "desc"
    });

    const {isLoading, users} = useSelector(state => state.Users);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(userAction.loadUsersRequested(query))
    }, [dispatch, query]);

    // Table columns
    const columns = [
        {
            title: 'First Name',
            dataIndex: 'firstName',
            key: 'firstName',
            width: '150px',
            render: (text, row) => {
                return (<p>{row.firstName}</p>);
            },
        },
        {
            title: 'Last Name',
            dataIndex: 'lastName',
            key: 'lastName',
            width: '150px',
            render: (text, row) => {
                return (<p>{row.lastName}</p>);
            },
        },
        {
            title: 'User Name',
            dataIndex: 'username',
            key: 'username',
            width: '150px',
            render: (text, row) => {
                return (<Tag color="lime">{row.username}</Tag>);
            },
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            width: '150px',
            render: (text, row) => {
                return (<Tag color="lime">{row.email}</Tag>);
            },
        },
        {
            title: 'Actions',
            key: 'action',
            width: '100px',
            className: 'noWrapCell',
            render: (text, row) => {
                return (
                    <div className="action-wrapper">
                        <button className="button button__icon">
                            <Icon type="edit" />
                        </button>
                        <Popconfirm title="Are you sure to delete this category？"
                                    okText="Yes"
                                    cancelText="No"
                                    placement="topRight">
                            <button className="button button__icon--danger">
                                <Icon type="delete" />
                            </button>
                        </Popconfirm>
                    </div>
                );
            },
        },
    ];

    return (
        <div className="page-content">
            <div className="page-content__title">
                <div className="page-content__title--text">
                    Users
                </div>
                <div className="page-content__title--action">
                    <button className="btn-action" ><Icon type="file-add" /> New</button>
                </div>
            </div>
            <div className="page-content__body">
                <div className="ant-table-responsive">
                    <Table dataSource={users} columns={columns} loading={isLoading} rowKey="id"
                           pagination={{
                               defaultPageSize: 5,
                               showSizeChanger: true,
                               defaultCurrent: 0,
                               pageSizeOptions: ['3', '5', '10'],
                           }}
                    />
                </div>
            </div>
        </div>
    );
}
