import React from 'react';
import {Icon} from 'antd';

function Home() {
    return (
        <div className="page-content">
            <div className="page-content__title">
                <div className="page-content__title--text">
                    Movie List
                </div>
                <div className="page-content__title--action">
                    <button className="btn-action"><Icon type="file-add" /> New</button>
                </div>
            </div>
        </div>
    );
}

export default Home;
