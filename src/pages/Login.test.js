import React from "react";
import {shallow, mount} from 'enzyme';

import {cleanup, render} from '@testing-library/react'
import Login from './Login';
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store';

jest.mock('react-redux', () => ({
    useDispatch: () => {},
    useSelector: () => ({
      isLoading: false, isLoginSuccess: false
    })
}));

jest.mock('react-router', () => ({
    useHistory: () => {}
}));
describe('Login component', () => {
    it('should render login page title', () => {
        const wrapper = mount(
            <Login />,
        );
        expect(wrapper.find('.login__title--main').text()).toEqual('Welcome Back!');
    });
});
