import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useForm} from 'react-hook-form';
import bgRegister from '../assets/images/pages/bg-register.jpg';
import {Icon} from "antd";
import {Link} from 'react-router-dom';
import authAction from "../store/auth/action";
import {useHistory} from 'react-router';

export default function Register() {
    const {isLoading, isUsernameExist, isEmailExist, isRegisterSuccess} = useSelector(state => state.Auth);
    const dispatch = useDispatch();
    const {handleSubmit, register, errors} = useForm();
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [confirmPassword, setConfirmPassword] = useState(true);
    const history = useHistory();

    useEffect(() => {
        if (isRegisterSuccess) {
            history.push('/auth/login');
        }
    }, [isRegisterSuccess, history]);

    // check if username exist
    const checkUsernameExist = async (e) => {
        const target = e.target;
        if (target && target.value && target.value.length > 5) {
            if (username !== target.value) {
                await setUsername(target.value);
                dispatch(authAction.checkUsernameRequested(target.value));
            }
        }
    };

    // Check confirm password
    const checkConfirmPassword = async (values) => {
        if (values.rePassword !== values.password) {
            await setConfirmPassword(false);
        } else {
            await setConfirmPassword(true);
        }
    };

    // Check if email exist
    const checkEmailExist = async (e) => {
        const target = e.target;
        if (target && target.value && target.value.length > 5) {
            if (email !== target.value) {
                await setEmail(target.value);
                dispatch(authAction.checkEmailRequested(target.value));
            }
        }
    };

    // Submit user registration
    const registerUser = values => {
        if (!isUsernameExist && !isEmailExist && confirmPassword) {
            if (values.rePassword !== values.password) {
                setConfirmPassword(false);
            } else {
                setConfirmPassword(true);
                dispatch(authAction.registerRequested(values.firstName, values.lastName, values.username,
                    values.email, values.password));
            }
        }
    };

    return (
        <React.Fragment>
            <div className="register-page">
                <div className="card-register">
                    <div className="card-register__left">
                        <div className="register__title">
                          <span className="register__title--main">
                              Register Page
                          </span>
                            <span className="register__title--sub">
                              Let's Join
                          </span>
                        </div>
                        <div className="register__form">
                            <div className="register__group-input">
                                <input placeholder="First Name" name="firstName"
                                       className={`register__form--input mr-1 ${errors.firstName ? 'error-input' : ''}`}
                                       ref={register({
                                           required: 'Required',
                                       })}/>
                                <input placeholder="Last Name" name="lastName"
                                       className={`register__form--input ml-1 ${errors.lastName ? 'error-input' : ''}`}
                                       ref={register({
                                           required: 'Required',
                                       })}/>
                            </div>
                            <input placeholder="Username" name="username"
                                   className={`register__form--input ${errors.username || isUsernameExist ? 'error-input' : ''}`}
                                   onBlur={event => checkUsernameExist(event)}
                                   ref={register({
                                       validate: value => value.length > 5
                                   })}/>
                            <input placeholder="Email" name="email"
                                   className={`register__form--input ${errors.email || isEmailExist ? 'error-input' : ''}`}
                                   onBlur={event => checkEmailExist(event)}
                                   ref={register({
                                       required: 'Required',
                                       value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                   })}/>
                            <input type="password" placeholder="Password" name="password"
                                   className={`register__form--input ${errors.password ? 'error-input' : ''}`}
                                   ref={register({
                                       validate: value => value.length > 5
                                   })}/>
                            <input type="password" placeholder="Confirm Password" name="rePassword"
                                   className={`register__form--input ${errors.rePassword || !confirmPassword ? 'error-input' : ''}`}
                                   onBlur={handleSubmit(checkConfirmPassword)}
                                   ref={register({
                                       validate: value => value.length > 5
                                   })}/>
                        </div>
                        <div className="register__action">
                            <button className="register__action--button"
                                    disabled={isLoading} onClick={handleSubmit(registerUser)}>
                                {isLoading && <Icon type="loading"/>} Register
                            </button>
                        </div>
                        <div className="register__message">
                            <p>Already have an account? <Link to="/auth/login">Login Here</Link></p>
                        </div>
                    </div>
                    <div className="card-register__right">
                        <img src={bgRegister} alt="Register Cover" className="card-register__right--image"/>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}
