import axios from 'axios';
import {defineHeaders} from "./auth.helper";

/*Find all movies*/
const findAllMovies = (query) => {
    return axios.post(`/api/movies/findAllMovies`, query, defineHeaders()).then(resp => {
        return resp.data;
    });
};

/*Find movie by id*/
const findMovieById = (movieId) => {
    return axios.get(`/api/movies/findById/${movieId}`, defineHeaders()).then(resp => {
        return resp.data;
    });
};

/*Create new movie*/
const createNewMovie = (movie) => {
    return axios.post('/api/movies/create', movie, defineHeaders()).then(resp => {
        return resp.data;
    });
};

/*Update movie*/
const updateMovie = (movie) => {
    return axios.put('/api/movies/update', movie, defineHeaders()).then(resp => {
        return resp.data;
    });
};

/*Remove movie*/
const removeMovie = (movieId) => {
    return axios.delete(`/api/movies/delete/${movieId}`, defineHeaders()).then(resp => resp.data);
};

export {
    findAllMovies,
    findMovieById,
    createNewMovie,
    updateMovie,
    removeMovie
}
