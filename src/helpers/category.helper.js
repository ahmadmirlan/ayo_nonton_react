import axios from 'axios';
import {defineHeaders} from "./auth.helper";

/*Get all categories*/
const getAllCategories = () => {
    return axios.get('/api/categories/getAll', {}).then(response => {
        return response.data;
    })
};

const createCategory = (category) => {
    return axios.post('/api/categories/create', category, defineHeaders()).then(resp => resp.data);
};

const updateCategory = (category) => {
    return axios.put('/api/categories/update', category, defineHeaders()).then(resp => resp.data);
};

const deleteCategory = (categoryId) => {
    return axios.delete(`/api/categories/delete/${categoryId}`, defineHeaders()).then(resp => resp.data);
};

export {
    getAllCategories,
    createCategory,
    updateCategory,
    deleteCategory
}
