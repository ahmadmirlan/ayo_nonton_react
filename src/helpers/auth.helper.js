import axios from 'axios';

const loginApi = (username, password) => {
    return axios.post('/api/auth/login', {username, password}).then(resp => {
        return resp.data;
    });
};

const registerApi = (firstName, lastName, username,  email, password) => {
    return axios.post('/api/auth/register', {firstName, lastName, username, email, password}).then(resp => {
        return resp.data;
    })
};

// Check username, if response status 200 it mean username available
const checkUsername = username => {
    return axios.post('/api/auth/checkUsername', {username}).then(resp => {
        return resp.data;
    });
};

// Check email, if response status 200 it mean username available
const checkEmail = email => {
    return axios.post('/api/auth/checkEmail', {email}).then(resp => {
        return resp.data;
    });
};

/*Find user by token*/
const findUserByToken = (token) => {
  return axios.get('/api/findUser/byToken', defineHeaders()).then(resp => resp.data);
};

/*Save user to local storage*/
const saveUser = (user) => {
    const token = user.accessToken;
    localStorage.setItem('yukToken', JSON.stringify(token));
    localStorage.setItem('user', JSON.stringify(user));
};

/*Get token from local storage*/
const getToken = () => {
    return JSON.parse(localStorage.getItem('yukToken'));
};

/*Get user from local storage*/
const getUser = () => {
    return JSON.parse(localStorage.getItem('user'));
};

// define headers
const defineHeaders = () => {
    const token = getToken();
    return {
        headers: {'Authorization': `Bearer ${token}`}
    };
};


export {
    loginApi,
    saveUser,
    getToken,
    getUser,
    registerApi,
    checkUsername,
    checkEmail,
    defineHeaders,
    findUserByToken
}
