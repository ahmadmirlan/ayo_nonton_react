import axios from 'axios';
import {defineHeaders} from './auth.helper';

// find all users
const findAllUsers = (query) => {
    return axios.post('/api/users/findAllUsers', query, defineHeaders()).then(resp => {
       return resp.data;
    });
};

export {
    findAllUsers
}
